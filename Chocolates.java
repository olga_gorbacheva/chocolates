package ru.god.chocolates;

import java.util.Scanner;

public class Chocolates {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите цену за одну шоколадку");
        int price = scanner.nextInt();
        System.out.println("Введите количество денег");
        int cash = scanner.nextInt();
        if (cash < price) {
            System.out.println("Извините, вам не хватает денег");
        } else {
            System.out.println("Введите количество оберток для обмена");
            int exchange = scanner.nextInt();
            if (exchange <= 1) {
                System.out.println("Штош, вы можете до бесконечности менять обертки на шоколадки");
            } else {
                System.out.println("Штош, на ваши денежки, с учетом обмена всех оберток, вы можете вынести из магазина " +
                        chocolates(cash, price, exchange) + " шоколадок");
            }
        }
    }

    private static int chocolates(int cash, int price, int exchange) {
        int chocolates = cash / price;
        int wrap = chocolates;
        while (wrap >= exchange) {
            chocolates += wrap / exchange;
            wrap = wrap / exchange + wrap % exchange;
        }
        return chocolates;
    }
}
